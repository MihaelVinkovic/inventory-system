﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryUIManager : MonoBehaviour {

    public Transform itemsParent;

    Inventory inventory;

    InventorySlotManager[] slots;


	// Use this for initialization
	void Start () {
        inventory = Inventory.inventory;
        inventory.InventoryDatabaseUpdate += UpdateInventoryUI;


        slots = itemsParent.GetComponentsInChildren<InventorySlotManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    void UpdateInventoryUI()
    {
        Debug.Log("Updating UI");      
        

        for(int i = 0; i < slots.Length; i++)
        {
            if(i < inventory.itemsInInventory.Count)
            {                
                slots[i].AddItem(inventory.itemsInInventory[i]);
            }
            else
            {
                slots[i].RemoveItem();
            }
        }

    }
}
