﻿using UnityEngine;
using UnityEngine.EventSystems;    
using UnityEngine.UI;

public class InventorySlotManager : MonoBehaviour, IPointerClickHandler {

    Item item;
    public Image icon;


	public void AddItem(Item itemToAdd)
    {
        item = itemToAdd;
        icon.sprite = item.image;
        icon.enabled = true;

        Color temp = icon.color;
        temp.a = 1f;
        icon.color = temp;
    }


    public void RemoveItem()
    {
        item = null;
        icon.sprite = null;
        icon.enabled = false;

        Color temp = icon.color;
        temp.a = 0f;
        icon.color = temp;
    }


    public void UseItem()
    {
        if (item != null)
        {
            Debug.Log(item.name);
            if (clicked)
            {
                item.Use();
                clicked = false;
            }
        }
        else
        {
            Debug.Log("There is no item in slot !");
        }
    }

    private bool clicked = false;

    public void OnPointerClick(PointerEventData eventdata)
    {
        if (eventdata.button == PointerEventData.InputButton.Right)
        {
            clicked = true;
            UseItem();
        }
#if UNITY_ANDROID
        if(eventdata.button == PointerEventData.InputButton.Left)
        {
            clicked = true;
            UseItem();
        }
#endif
    }
}
