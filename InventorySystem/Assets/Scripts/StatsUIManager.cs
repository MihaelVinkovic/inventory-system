﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsUIManager : MonoBehaviour {

    EquipmentController equipmentController;
    PlayerAtributes playerAtributes;
        

    public Text StrengthText, DurabilityText, ShieldText, SpeedText;


	// Use this for initialization
	void Start () {
        equipmentController = EquipmentController.equipmentController;
        equipmentController.statsUpdateUI += UpdateUI;


        playerAtributes = PlayerAtributes.playerAtributes;


        UpdateUI();
    }
	
	// Update is called once per frame
	void Update () {
		
	}


    void UpdateUI()
    {
        StrengthText.text = "STRENGTH: " + playerAtributes.Strength.ToString();
        DurabilityText.text = "DURABILITY: " + playerAtributes.Durability.ToString();
        ShieldText.text = "SHIELD: " + playerAtributes.Shield.ToString();
        SpeedText.text = "SPEED: " + playerAtributes.Speed.ToString();
    }



}
