﻿using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour {


    private void Start()
    {
        Inventory.SetActive(false);
        EquipScreen.SetActive(false);
        StatsWindow.SetActive(false);
    }

    private void Update()
    {

        if(Inventory.activeSelf || EquipScreen.activeSelf || StatsWindow.activeSelf)
        {
            Time.timeScale = 0;
            spawnButton.SetActive(false);
        }
        else
        {
            Time.timeScale = 1;
            spawnButton.SetActive(true);
        }


        if (Input.GetButtonDown("Inventory"))
        {
            if (Inventory.activeSelf)
            {
                CloseInventory();
            }
            else
            {
                ShowInventory();
            }
        }


        if (Input.GetButtonDown("Equip"))
        {
            if (EquipScreen.activeSelf)
            {
                CloseEquipScreen();
            }
            else
            {
                ShowEquipScreen();
            }
        }



        if (Input.GetButtonDown("Stats"))
        {
            if (StatsWindow.activeSelf)
            {
                CloseStatScreen();
            }
            else
            {                
                ShowStatsScreen();
            }
        }
    }



#region INVENTORY
    public GameObject ShowInventoryButton;
    public GameObject Inventory;
    
    public void ShowInventory()
    {
        ShowInventoryButton.SetActive(false);
        Inventory.SetActive(true);
        //Time.timeScale = 0;
    }

    public void CloseInventory()
    {
        Inventory.SetActive(false);
        ShowInventoryButton.SetActive(true);
        if (!EquipScreen.activeSelf)
        {
          //  Time.timeScale = 1;
        }
    }

    #endregion

#region EQUIP
    public GameObject ShowEquipButton;
    public GameObject EquipScreen;

    public void ShowEquipScreen()
    {
        ShowEquipButton.SetActive(false);
        EquipScreen.SetActive(true);
        //Time.timeScale = 0;
    }
    
    public void CloseEquipScreen()
    {
        ShowEquipButton.SetActive(true);
        EquipScreen.SetActive(false);
        if (!Inventory.activeSelf)
        {
          //  Time.timeScale = 1;
        }
    }


    #endregion

#region STATS

    public GameObject ShowStatsWindowButton;
    public GameObject StatsWindow;

    public void ShowStatsScreen()
    {
        StatsWindow.SetActive(true);
        ShowStatsWindowButton.SetActive(false);
    }


    public void CloseStatScreen()
    {
        StatsWindow.SetActive(false);
        ShowStatsWindowButton.SetActive(true);
    }

    #endregion

    #region SPAWN

    private int xMin = 6, xMax = 12;
    private int yMin = 19, yMax = 24;

    public GameObject spawnButton;
    public GameObject[] randomGameObject;

    public void SpawnRandomItemOnRandomPos()
    {
        int rnd = Random.Range(0, randomGameObject.Length);
        Vector2 randomPos = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));

        Instantiate(randomGameObject[rnd], randomPos, Quaternion.identity);
    }





#endregion


}
