﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PlayerAtributes : MonoBehaviour{

    public int Strength = 10;
    public int Durability = 10;
    public int Shield = 10; 
    public int Speed = 10;



    public static PlayerAtributes playerAtributes;

    private void Awake()
    {
        playerAtributes = this;
    }  



    public void AddStrength(int x)
    {
        if(x < 10)
        {
            Strength = Strength + x;
        }
    }

    public  void RemoveStrenght(int y)
    {
        if(y < Strength)
        {
            Strength = Strength - y;
        }
    }



    public void AddDurability(int x)
    {
        if (x < 10)
        {
            Durability = Durability + x;
        }
    }

    public void RemoveDurability(int y)
    {
        if (y < Durability)
        {
            Durability = Durability - y;
        }
    }

    public void AddShield(int x)
    {
        if (x < 10)
        {
            Shield = Shield + x;
        }
    }

    public void RemoveShield(int y)
    {
        if (y < Shield)
        {
            Shield = Shield - y;
        }
    }

    public void AddSpeed(int x)
    {
        if (x < 10)
        {
            Speed = Speed + x;
        }
    }

    public void RemoveSpeed(int y)
    {
        if (y < Speed)
        {
            Speed = Speed - y;
        }
    }





}
