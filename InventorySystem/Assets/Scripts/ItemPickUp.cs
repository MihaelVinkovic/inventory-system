﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ItemPickUp : MonoBehaviour {

    public Item item;

    private SpriteRenderer icon;
   

	// Use this for initialization
	void Start () {
        icon = GetComponent<SpriteRenderer>();
        icon.sprite = item.image;

	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {        
        if (Inventory.inventory.AddItemToInventory(item))
        {
            Debug.Log(item.name + " has been picked up.");
            Destroy(gameObject);
        }
        
    }
}
