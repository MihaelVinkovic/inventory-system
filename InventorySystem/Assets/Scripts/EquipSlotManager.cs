﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EquipSlotManager : MonoBehaviour, IPointerClickHandler
{


    EquipableItem equipableItem;

    public SlotState slotState;
    public Image equipSlotImage;
    public Text equipSlotName;




    public void AddEquipableItem(EquipableItem itemToAdd)
    {
        equipableItem = itemToAdd;
        slotState = SlotState.Busy;

        equipSlotImage.sprite = equipableItem.image;
        equipSlotName.text = equipableItem.equipSlot.ToString().ToUpper();
    
        Color temp = equipSlotImage.color;
        temp.a = 1f;
        equipSlotImage.color = temp;
    }

    private bool clicked = false;

    public void UneqipItem()
    {
        if (clicked)
        {
            slotState = SlotState.Free;
            equipSlotImage.sprite = null;
            Color temp = equipSlotImage.color;
            temp.a = 0f;
            equipSlotImage.color = temp;
            clicked = false;
            EquipmentController.equipmentController.UnEquip(equipableItem);            
        }
    }

    public void OnPointerClick(PointerEventData eventdata)
    {
        if (eventdata.button == PointerEventData.InputButton.Right)
        {
            clicked = true;
            UneqipItem();            
        }

#if UNITY_ANDROID

        if(eventdata.button == PointerEventData.InputButton.Left)
        {
            clicked = true;
            UneqipItem();
        }
#endif
    }

}


public enum SlotState { Free, Busy}
