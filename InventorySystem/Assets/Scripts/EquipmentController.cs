﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipmentController : MonoBehaviour {

    public static EquipmentController equipmentController;    

    private void Awake()
    {
        equipmentController = this;
    }


    public delegate void StatsUpdate();
    public StatsUpdate statsUpdateUI;


    [HideInInspector]
    public EquipableItem[] currentEquipedItems;

    Inventory inventory;
    PlayerAtributes playerAtributes;

    [HideInInspector]
    public EquipSlotManager[] equipSlotManager;


    public Transform parent;

	// Use this for initialization
	void Start () {
        InitalizeArrayLength();
        inventory = Inventory.inventory;
        playerAtributes = PlayerAtributes.playerAtributes;
	}


    private void InitalizeArrayLength()
    {
        int length = System.Enum.GetNames(typeof(EquipSlot)).Length;
        currentEquipedItems = new EquipableItem[length];

        equipSlotManager = new EquipSlotManager[length];
        equipSlotManager = parent.GetComponentsInChildren<EquipSlotManager>();
    }

    public void Equip(EquipableItem item)
    {
        int index = (int)item.equipSlot;
        EquipableItem oldItem = currentEquipedItems[index];
        
        if (currentEquipedItems[index] != null)
        {
            RemoveModifiers(oldItem);
            AddModifiers(item);
            //Debug.Log("Cannot Equip!");
            //Debug.Log("Old item: " + oldItem.name);
            currentEquipedItems[index] = item;
            //Debug.Log("New item: " + item.name);
            //Debug.Log(currentEquipedItems[index].name);
            equipSlotManager[index].AddEquipableItem(item);
            inventory.RemoveItemFromInventory(item);
            inventory.AddItemToInventory(oldItem);
        }
        else
        {
            currentEquipedItems[index] = item;
            equipSlotManager[index].AddEquipableItem(item);
            inventory.RemoveItemFromInventory(item);
            AddModifiers(item);
            Debug.Log("Shield Modifier" + item.shieldModifier);
            //Debug.Log(index);

        }

        


    }

    public void UnEquip(EquipableItem item)
    {
        int index = (int)item.equipSlot;
        inventory.AddItemToInventory(item);
        currentEquipedItems[index] = null;
        RemoveModifiers(item);
    }


    public void AddModifiers(EquipableItem item)
    {
        playerAtributes.AddStrength(item.strengthModifier);
        playerAtributes.AddDurability(item.durabilityModifier);
        playerAtributes.AddShield(item.shieldModifier);
        playerAtributes.AddSpeed(item.speedModifier);        
        statsUpdateUI.Invoke();
    }

    public void RemoveModifiers(EquipableItem item)
    {
        playerAtributes.RemoveStrenght(item.strengthModifier);
        playerAtributes.RemoveDurability(item.durabilityModifier);
        playerAtributes.RemoveShield(item.shieldModifier);
        playerAtributes.RemoveSpeed(item.speedModifier);
        statsUpdateUI.Invoke();
    }



	
	// Update is called once per frame
	void Update () {
		
	}

 
}
