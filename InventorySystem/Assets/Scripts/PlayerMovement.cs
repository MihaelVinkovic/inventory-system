﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class PlayerMovement : MonoBehaviour {

    public float speed;
    public float maxSpeed = 10f;

    private Rigidbody2D rigidBody;
    private GameObject mainCamera;
    private Animator animator;

    private Vector3 cameraOffset = new Vector3(0,0,-2);


	// Use this for initialization
	void Start () {
        rigidBody = GetComponent<Rigidbody2D>();
        mainCamera = GameObject.Find("Camera");
        animator = GetComponent<Animator>();
	}


 

    // Update is called once per frame
    void FixedUpdate ()
    {

        // Camera following player in 2D world spaces
        mainCamera.transform.position = transform.position + cameraOffset;


#if UNITY_ANDROID
              
        float horiz = CrossPlatformInputManager.GetAxis("Horizontal");
        float verti = CrossPlatformInputManager.GetAxis("Vertical");

        SetAnimation(horiz, verti);

        Vector2 movementAndroid = new Vector2(horiz, verti);
        movementAndroid.Normalize();

        rigidBody.AddForce(movementAndroid * speed);

        if (rigidBody.velocity.magnitude > maxSpeed)
        {
            rigidBody.velocity = rigidBody.velocity.normalized * maxSpeed;
        }
#endif




        float horizontalMovement = Input.GetAxis("Horizontal");        
        float verticalMovement = Input.GetAxis("Vertical");
        
        SetAnimation(horizontalMovement, verticalMovement);

        Vector2 movement = new Vector2(horizontalMovement, verticalMovement);
        movement.Normalize();

        rigidBody.AddForce(movement * speed);



        if (rigidBody.velocity.magnitude > maxSpeed)
        {
            rigidBody.velocity = rigidBody.velocity.normalized * maxSpeed;
        }

    }


    private void SetAnimation(float h, float v)
    {
        if (h > 0)
        {
            //goes right
            animator.SetTrigger("WalkRight");            
        }

        if(h < 0)
        {
            //goes left
            animator.SetTrigger("WalkLeft");           
        }

        if (v > 0)
        {
            //goes up
            animator.SetTrigger("WalkUp");
        }

        if (v < 0)
        {
            //goes down
            animator.SetTrigger("WalkDown");
        }

        if (h == 0 && v == 0)
        {
            //idle
            animator.SetTrigger("PlayerIdle");
        }


    }

}
