﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour {

    public static Inventory inventory;

    public delegate void InventoryDatabaseUpdated();
    public InventoryDatabaseUpdated InventoryDatabaseUpdate;

    private int maxSpaceInInventory = 32;

    public List<Item> itemsInInventory = new List<Item>();

    EquipmentController equipmentController;

    private void Awake()
    {
        inventory = this;
    }

    private void Start()
    {
        equipmentController = EquipmentController.equipmentController;
    }

    public bool AddItemToInventory(Item item)
    {
        if (NotEquiped(item))
        {
            InventoryDatabaseUpdate.Invoke();
            return true;
        }
        if(itemsInInventory.Count >= maxSpaceInInventory)
        {

            Debug.Log("Inventory is full!");
            return false;

        }
        else
        {

            // Add item to Inventory
            itemsInInventory.Add(item);

            // Update Inventory UI
            InventoryDatabaseUpdate.Invoke();
            return true;

        }
    }

    public void RemoveItemFromInventory(Item item)
    {

        // Remove this idem from list
        itemsInInventory.Remove(item);

        // Update Inventory UI
        InventoryDatabaseUpdate.Invoke();

    }


    public bool NotEquiped(Item itemm)
    {
        EquipableItem item = (EquipableItem)itemm;
        if(equipmentController.currentEquipedItems[(int)item.equipSlot] == null)
        {
            equipmentController.Equip(item);
            return true;
        }



        return false;
    }
}
