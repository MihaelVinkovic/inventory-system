﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="Equipable Item", menuName ="Equipable Item")]
public class EquipableItem : Item {

    public EquipSlot equipSlot;

    public int strengthModifier;
    public int durabilityModifier;
    public int shieldModifier;
    public int speedModifier;


    public override void Use()
    {
        base.Use();
        EquipmentController.equipmentController.Equip(this);
    }

}

public enum EquipSlot { Head, Torso, Hands, Legs }